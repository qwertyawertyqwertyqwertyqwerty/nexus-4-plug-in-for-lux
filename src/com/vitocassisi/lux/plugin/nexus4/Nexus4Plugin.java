package com.vitocassisi.lux.plugin.nexus4;

import com.vitocassisi.lux.plugin.PassiveDisplay;

public class Nexus4Plugin extends PassiveDisplay {

	@Override
	public String[] setRGB(int r, int g, int b) {
		//superuser commands to set and apply RGB values
		return new String[] {
				"chmod 666 /sys/devices/platform/kcal_ctrl.0/kcal",
				"chmod 666 /sys/devices/platform/kcal_ctrl.0/kcal_ctrl",
				"echo \"" + Math.round((r / 255f) * 238) + " "
						+ Math.round((g / 255f) * 228) + " "
						+ Math.round((b / 255f) * 240)
						+ "\" > /sys/devices/platform/kcal_ctrl.0/kcal",
				"echo 1 > /sys/devices/platform/kcal_ctrl.0/kcal_ctrl" };
	}

	@Override
	public String[] setBacklightLevel(int brightness) {
		//superuser commands to set backlight value
		return new String[] {
				"chmod 666 /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/brightness",
				"echo \""
						+ brightness
						+ "\" > /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/brightness",
				"chmod 444 /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/brightness" };
	}

	@Override
	public String[] setMaxBacklight(int max) {
		//superuser commands to set max backlight value
		return new String[] {
				"chmod 666 /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/max_brightness",
				"echo \""
						+ max
						+ "\" > /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/max_brightness" };
	}

	@Override
	public String[] setButtonLightLevel(int brightness) {
		//Nexus 4 doesn't have physical buttons
		return null;
	}

	@Override
	public LuxBundle getButtonLightLevel() {
		//Nexus 4 doesn't have physical buttons
		return null;
	}

	@Override
	public LuxBundle getBacklightLevel() {
		//superuser instructions to get current backlight level
		return LuxBundle
				.builder(
						"/sys/devices/platform/msm_fb.525825/leds/lcd-backlight/brightness",
						DELIMITED_NONE, 0, 255);
	}

	@Override
	public LuxBundle getMaxBacklight() {
		//superuser instructions to get current max backlight level
		return LuxBundle
				.builder(
						"/sys/devices/platform/msm_fb.525825/leds/lcd-backlight/max_brightness",
						DELIMITED_NONE, 0, 255);
	}

	@Override
	public LuxBundle getRGB() {
		//superuser instructions to get current RGB values
		return LuxBundle.builder("/sys/devices/platform/kcal_ctrl.0/kcal",
				DELIMITED_SPACE, 0, 255);
	}

	@Override
	public String[] canCauseSysIssues() {
		//No known issues
		return null;
	}

	@Override
	public boolean isSupportedDevice(String model, String kernel) {
		//ignore kernel since this plugin works with all root enabled Nexus 4's.
		if (model.equals("Nexus 4")) {
			return true;
		}
		return false;
	}

	@Override
	public LuxBundle getLuxValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] onCleanup() {
		//commands which should be run when the plug-in is disabled/removed
		String[] onClean = super.onCleanup();
		String[] temp = new String[onClean.length + 1];
		System.arraycopy(onClean, 0, temp, 0, onClean.length);
		temp[onClean.length] = "chmod 666 /sys/devices/platform/msm_fb.525825/leds/lcd-backlight/brightness";
		return temp;
	}
}
